package org.sytac.test.automation;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AssignmentSecretTest extends AssignmentBaseTest {

    static Stream<Arguments> assignmentInputSource() {
        return Stream.of(
                Arguments.of(10, 5),
                Arguments.of(0, 0),
                Arguments.of(5, 2));
    }

    @ParameterizedTest
    @MethodSource("assignmentInputSource")
    void process(int input, int output) {

        int result = assignment.process(input);

        assertEquals(output, result);
    }
}
