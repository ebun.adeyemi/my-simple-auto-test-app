package org.sytac.test.automation;

import org.junit.jupiter.api.BeforeEach;
import org.sytac.test.internal.Assignment;

import java.util.ServiceLoader;

abstract class AssignmentBaseTest {
    Assignment assignment;

    @BeforeEach
    void setUp() {
        assignment = ServiceLoader.load(Assignment.class).findFirst().orElseThrow();
    }
}
